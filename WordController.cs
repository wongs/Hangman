﻿
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Hangman
{
    public class WordController
    {
        private readonly IConfiguration _config;
        private string _hangmanWord;
        private readonly Dictionary<int, char> _alphabet = new Dictionary<int, char>();
        private Dictionary<int, char> _guessedAlphabet = new Dictionary<int, char>();

        // Define chars to trim
        private char[] charsToTrim = new char[] { '[', ']', '"' };

        public WordController(IConfiguration config)
        {
            _config = config;
            GetAlphabetDictionary();

            if (!GetWordFromAPI())
            {
                // error getting word on api
                GetWordFromJSON();
            }

            TrimWord();
        }

        public string HangmanWord
        {
            get
            {
                return _hangmanWord;
            }
        }

        public bool CheckInput(char input)
        {
            var key = _alphabet.FirstOrDefault(x => x.Value == input).Key;

            if (_guessedAlphabet.ContainsKey(key))
            {
                return true;
            }
            else
            {
                _guessedAlphabet.Add(key, input);
                return false;
            }
        }

        private bool GetWordFromAPI()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    // source https://github.com/RazorSh4rk/random-word-api
                    var response = client.GetAsync("https://random-word-api.herokuapp.com/word?number=1").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var responseContent = response.Content;
                        _hangmanWord = responseContent.ReadAsStringAsync().Result;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
                return true;
            }
        }

        private void GetWordFromJSON()
        {
            var words = _config.GetSection("offlineWords").Get<string[]>();
            var rnd = new Random();

            _hangmanWord = words[rnd.Next(0, words.Length)];
        }

        private void TrimWord()
        {
            _hangmanWord = _hangmanWord.Trim(charsToTrim);
        }

        private void GetAlphabetDictionary()
        {
            for (char c = 'a'; c <= 'z'; c++)
            {
                int key = c - 'a' + 1;
                _alphabet.Add(key, c);
            }
        }
    }

}
