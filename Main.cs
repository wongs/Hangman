using Hangman;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

public class Program
{
    static void Main (string[] args)
    {
        ConsoleKeyInfo cki;

        using IHost host = Host.CreateDefaultBuilder(args).Build();
        IConfiguration config = host.Services.GetRequiredService<IConfiguration>();

        int configGuesses = Int32.Parse(config.GetValue<string>("settings:guesses"));
        var hints = Convert.ToBoolean(config.GetValue<string>("settings:hints"));
        var hintsChars = config.GetValue<string>("settings:hintsChars").ToCharArray();

        do
        {
            Console.WriteLine("Get Hangman word from API ...");
            // get word drom api
            var wordController = new WordController(config);
            Console.WriteLine("Got a word. Game can start now.");

            LoopRound(wordController, hintsChars, configGuesses, hints);

            Console.WriteLine("Presss any key for another round or press ESC to exit.");
            cki = Console.ReadKey();
        }
        while (cki.Key != ConsoleKey.Escape);
    }

    private static void LoopRound(WordController wc, char[] hints, int guesses, bool withHints = true)
    {
        var wordChosen = wc.HangmanWord;
        int guessesLeft = guesses;
        char userInput;
        char[] dashes;
        // Generate dashes, convert to string
        if (withHints)
        {
            dashes = ConvertToDashes(wordChosen, hints);
        }
        else
        {
            dashes = ConvertWithoutHints(wordChosen);
        }
        string dashesToString = new string(dashes);

        // print init game start
        StartGame(dashesToString);

        while (true)
        {
            Console.WriteLine();
            // Grab user input, check if parsing is possible
            var userInputString = Console.ReadLine().ToLower();

            // If user enters full word, set dashes equal to chosen word and break
            if (userInputString == wordChosen)
            {
                dashesToString = wordChosen;
                break;
            }
            else if (userInputString == "hints")
            {
                dashesToString = ActivateHints(dashesToString, wordChosen, hints);
                dashes = dashesToString.ToCharArray();
                Console.WriteLine("Hints activated.");
                Console.WriteLine(dashesToString);
                continue;
            }
            else if (userInputString == "exit")
            {
                Environment.Exit(0);
            }
            else if (userInputString.Length != 1)
            {
                Console.Clear();
                Console.WriteLine("Please enter 1 letter!");
                // Loop back if parsing not possible
                continue;
            }

            // Parse string to char
            userInput = char.Parse(userInputString);

            if (wc.CheckInput(userInput))
            {
                // already tried
                Console.Clear();

                Console.WriteLine(dashesToString);
                Console.WriteLine();
                Console.WriteLine(string.Format("Letter '{0}' already tried.", userInput));
                continue;
            }

            // Compare guess to every character in selected word
            for (int i = 0; i < wordChosen.Length; i++)
            {
                // If input equal to char of chosen word determined by i, change corresponding char in dashes[]
                if (wordChosen[i] == userInput)
                {
                    dashes[i] = userInput;
                }
            }

            // See if word contains char at all, subtract incorrect guesses as needed
            if (wordChosen.Contains(userInput) == false)
            {
                guessesLeft -= 1;
            }

            // Convert char array to string so we can compare it to the chosen word
            dashesToString = new string(dashes);

            // if guessing is complete or if you've run out of guesses, break loop
            if (dashesToString == wordChosen || guessesLeft == 0)
            {
                break;
            }

            // Clear console screen
            Console.Clear();

            //Console.WriteLine();
            Console.WriteLine(dashesToString);
            Console.WriteLine();
            Console.WriteLine(guessesLeft + " incorrect guesses left.");
        }

        if (dashesToString == wordChosen)
        {
            var msg = string.Format("Congratulations! \nThe word was: {0}.\nYou had {1} incorrect guesses left.", wordChosen, guessesLeft);
            Console.WriteLine(msg);
        }
        else if (guessesLeft == 0)
        {
            var msg = string.Format("Sorry, you've run out of incorrect guesses. \nThe word was: {0}.", wordChosen);
            Console.WriteLine(msg);
        }
    }

    private static void StartGame(string dashes)
    {
        // Prompt user & display dashes, last execution before while loop
        Console.WriteLine("Welcome to Hangman! \nPlease enter one character at a time, or the entire word. \nThere are no numbers or punctuation. \nYou have already been given the letters RSTLNE. \nTo exit, click the x button on the window, or type CTRL + C at any time. \nGood luck, and have fun!");
        Console.WriteLine();
        Console.WriteLine(dashes);
    }

    private static char[] ConvertToDashes(string word, char[] hints)
    {
        var converted = new char[word.Length];

        for (int i = 0; i < word.Length; i++)
        {
            converted[i] = '-';
        }

        for (int i = 0; i < word.Length; i++)
        {
            for (int x = 0; x < hints.Length; x++)
            {
                if (word[i] == hints[x])
                {
                    converted[i] = hints[x];
                }
            }
        }
        return converted;
    }

    private static char[] ConvertWithoutHints(string word)
    {
        var converted = new char[word.Length];
        for (int i = 0; i < word.Length; i++)
        {
            converted[i] = '-';
        }
        return converted;
    }

    private static string ActivateHints(string dashes, string word, char[] hints)
    {
        for (int i = 0; i < word.Length; i++)
        {
            for (int x = 0; x < hints.Length; x++)
            {
                if (word[i] == hints[x])
                {
                    dashes = dashes.Remove(i, 1);
                    dashes = dashes.Insert(i, hints[x].ToString());
                }
            }
        }
        return dashes;
    }
}
